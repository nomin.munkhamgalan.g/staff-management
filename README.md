# Staff Management

# environment variable for sqlite in .env file
DATABASE_URL="file:./dev.db"

# for update prisma schema db
npx prisma generate

# for update db
npx prisma migrate dev
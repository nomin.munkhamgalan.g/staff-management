import { ApolloServer } from '@apollo/server';
import { expressMiddleware } from '@apollo/server/express4';
import { ApolloServerPluginDrainHttpServer } from '@apollo/server/plugin/drainHttpServer';
import express from 'express';
import http from 'http';
import cors from 'cors';
import { PrismaClient } from '@prisma/client';

const app = express();
const httpServer = http.createServer(app);
const prisma = new PrismaClient();

const typeDefs = `#graphql

  type Account {
    id: Int!
    username: String!
    password: String!
  }

  type Staff {
    id: Int!
    name: String
    email: String
    phone: String
    address: String
    birthdate: String
    gender: String
  }

  input StaffInput {
    name: String
    email: String
    phone: String
  }

  type Query {
    accounts: [Account]
    staffs: [Staff]
  }

  type Mutation {
    staff(input: StaffInput): Staff
  }
`;

const resolvers = {
  Query: {
    accounts: () => {
      return prisma.account.findMany();
    },
    staffs: () => {
      return prisma.staff.findMany();
    }
  },
  Mutation: {
    staff: async (parent, args) => {
      console.log(args);
      const stf = await prisma.staff.create({
        data: {
          name: args.input.name,
          email: args.input.email,
          phone: args.input.phone
        }
      });
      return stf;
    }
  }
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  plugins: [ApolloServerPluginDrainHttpServer({httpServer})]
});

await server.start();
app.use('/graphql',
   cors<cors.CorsRequest>({origin: '*'}),
   express.json(),
   expressMiddleware(server)
);

await new Promise<void>((resolve) => httpServer.listen({port: 4000}, resolve));
console.log(`Server ready at: http://localhost:4000/graphql`);